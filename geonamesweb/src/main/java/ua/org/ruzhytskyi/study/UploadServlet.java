package ua.org.ruzhytskyi.study;

import java.io.*;
import java.util.*;

import java.lang.Iterable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.*;

import ua.org.ruzhytskyi.study.ImportData;


public class UploadServlet extends HttpServlet {
    private boolean isMultipart;
    private String filePath;
    private int maxFileSize = 50 * 1024;
    private int maxMemSize = 4 * 1024;
    private File file ;

    public void init(){
        filePath = getServletContext().getInitParameter("file-upload");
        try (Connection conn = ImportData.getJNDIConnection()) {
            ImportData.createParent(conn);
        } catch (Exception e) {
            System.out.println("Failed to init servlet");
        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
            throws ServletException, java.io.IOException {
        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        if(!isMultipart){
            request.setAttribute("status", "not_a_file");
            request.getRequestDispatcher("/upload.jsp").forward(
                request, response);
            return;
        }
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);
        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File(filePath));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        try { 
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);
    
            // Process the uploaded file items
            Iterator item = fileItems.iterator();

            while (item.hasNext()) {
                FileItem fileItem = (FileItem)item.next();
                if (!fileItem.isFormField()) {
                    String fileName = fileItem.getName();
                    System.out.println(fileName);
                    fileName = fileName.split("\\.")[0];
                    System.out.println(fileName);
                    InputStreamReader reader = new InputStreamReader(
                        fileItem.getInputStream());

                    Iterable<CSVRecord> records = null;
                    try {
                        records = ImportData.parseCSV(reader);
                    } catch (IOException e) {
                        String msg = "Failed to parse file";
                        System.out.println();
                        respond(request, response, "failed", msg);
                        return;
                    }

                    try (Connection conn = ImportData.getJNDIConnection()) {
                        ImportData.reCreateTable(conn, fileName);
                        ImportData.populateTable(conn, records, fileName); 
                    } catch (SQLException e) {
                        String msg = "Failed to populate table";
                        System.out.println(msg);
                        respond(request, response, "failed", msg);
                        return;
                    }
                    respond(request, response, "imported");
                    return;
                }
            }
        } catch(Exception ex) {
            System.out.println(ex);
            respond(request, response, "failed", ex.toString());
            return;
        }
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, java.io.IOException {
        throw new ServletException("GET method used with " +
                    getClass( ).getName( )+": POST method required.");
    }

    public void respond(HttpServletRequest request,
                        HttpServletResponse response,
                        String status) 
            throws ServletException, IOException {
        request.setAttribute("status", status);
        request.getRequestDispatcher("/upload.jsp").forward(request, response);
    }

    public void respond(HttpServletRequest request,
                        HttpServletResponse response,
                        String status,
                        String message)
            throws ServletException, IOException {
        request.setAttribute("message", message);
        respond(request, response, status);
    }
}
