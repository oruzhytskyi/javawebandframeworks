package ua.org.ruzhytskyi.study;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.Iterable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class ImportData {
    public static String DATASOURCE_CONTEXT = "jdbc/StudyDB";
    public static String parentTableName = "geonames";

    public static Connection getJNDIConnection() {
        Connection result = null;
        try {
            Context initialContext = new InitialContext();
            if (initialContext == null){
                System.out.println("JNDI problem. Cannot get InitialContext.");
            }
            DataSource datasource = (DataSource) initialContext.lookup(
                                    DATASOURCE_CONTEXT);
            if (datasource != null) {
                result = datasource.getConnection();
            }
            else {
                System.out.println("Failed to lookup datasource.");
            }
        } catch (NamingException ex) {
            System.out.println("Cannot get connection: " + ex);
        } catch(SQLException ex){
            System.out.println("Cannot get connection: " + ex);
        }
        return result;
    }

    public static void createParent(Connection con) 
            throws SQLException {
        String createSql =
            "create table if not exists " + parentTableName + "(" +
            "id serial primary key, " +
            "country_code varchar(2), " +
            "postal_code varchar(20), " +
            "place_name varchar(180), " +
            "admin_name1 varchar(100), " +
            "admin_code1 varchar(20), " +
            "admin_name2 varchar(100), " +
            "admin_code2 varchar(20), " +
            "admin_name3 varchar(100), " +
            "admin_code3 varchar(20), " +
            "latitude varchar(50), " +
            "longitude varchar(50), " +
            "accuracy varchar(1));";

        System.out.println(createSql);

        Statement stmt = con.createStatement();
        stmt.executeUpdate(createSql);
    }

    public static void reCreateTable(Connection con, String tableName)
            throws SQLException {
        String dropSql = "drop table if exists " + tableName;
        String createSql =
            "create table " + tableName + "(" +
            "id serial primary key, " +
            "country_code varchar(2), " +
            "postal_code varchar(20), " +
            "place_name varchar(180), " +
            "admin_name1 varchar(100), " +
            "admin_code1 varchar(20), " +
            "admin_name2 varchar(100), " +
            "admin_code2 varchar(20), " +
            "admin_name3 varchar(100), " +
            "admin_code3 varchar(20), " +
            "latitude varchar(50), " +
            "longitude varchar(50), " +
            "accuracy varchar(1)) inherits(" +
            parentTableName + ");";

        System.out.println(createSql);

        Statement stmt = con.createStatement();
        stmt.executeUpdate(dropSql);
        stmt.executeUpdate(createSql);
    }

    public static void populateTable(
            Connection con,
            Iterable<CSVRecord> records,
            String tableName) throws SQLException {

        con.setAutoCommit(false);
        String insertSql =
            "insert into " + tableName +
            "(country_code, postal_code, place_name, admin_name1, admin_code1," +
            " admin_name2, admin_code2, admin_name3, admin_code3, latitude," +
            " longitude, accuracy)" +
            " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        PreparedStatement stmt = con.prepareStatement(insertSql);
        for (CSVRecord record: records) {
            stmt.setString(1, record.get(0)); 
            stmt.setString(2, record.get(1)); 
            stmt.setString(3, record.get(2)); 
            stmt.setString(4, record.get(3)); 
            stmt.setString(5, record.get(4)); 
            stmt.setString(6, record.get(5)); 
            stmt.setString(7, record.get(6)); 
            stmt.setString(8, record.get(7)); 
            stmt.setString(9, record.get(8)); 
            stmt.setString(10, record.get(9)); 
            stmt.setString(11, record.get(10)); 
            stmt.setString(12, record.get(11)); 
            stmt.addBatch();
        }
        stmt.executeBatch();
        con.commit();
    }

    public static Iterable<CSVRecord> parseCSV(InputStreamReader reader)
            throws IOException {
        BufferedReader in = new BufferedReader(reader);
        return CSVFormat.TDF.parse(in);
    }
}
