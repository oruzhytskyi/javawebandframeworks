<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <title>File Uploading Form</title>
    </head>
    <body>
        <h3>File Upload:</h3>
        <c:choose>
          <c:when test="${status == 'imported'}">
            <p>Import completed.</p>
          </c:when>
          <c:when test="${status == 'failed'}">
            <p>Import failed: "${message}"</p>
          </c:when>
          <c:otherwise>
            Select a file to upload: <br />
            <form action="UploadServlet" method="POST"
                        enctype="multipart/form-data">
                <input type="file" name="file" size="50" />
                <br />
                <input type="submit" value="Upload File" />
            </form>
          </c:otherwise>
        </c:choose>
    </body>
</html>
