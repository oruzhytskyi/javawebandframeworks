<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<html>
    <head>
    </head>
    <body>
        <sql:setDataSource dataSource="jdbc/StudyDB" />
        <h2>Search by country</h2>
        <c:choose>
          <c:when test="${'POST'.equalsIgnoreCase(pageContext.request.method)}">
            <sql:query var="result" >
              select * from geonames where
                country_code like '%<c:out value="${param.code}"/>%' and
                place_name like '%<c:out value="${param.name}"/>%'
            </sql:query>
            <table border="1" width="100%">
              <tr>
                <th>Country code</th>
                <th>Postal code</th>
                <th>Place name</th>
                <th>Admin name 1</th>
                <th>Admin code 1</th>
                <th>Admin name 2</th>
                <th>Admin code 2</th>
                <th>Admin name 3</th>
                <th>Admin code 3</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Accuracy</th>
              </tr>
              <c:forEach var="row" items="${result.rows}">
              <tr>
                <td><c:out value="${row.country_code}"/></td>
                <td><c:out value="${row.postal_code}"/></td>
                <td><c:out value="${row.place_name}"/></td>
                <td><c:out value="${row.admin_name1}"/></td>
                <td><c:out value="${row.admin_code1}"/></td>
                <td><c:out value="${row.admin_name2}"/></td>
                <td><c:out value="${row.admin_code2}"/></td>
                <td><c:out value="${row.admin_name3}"/></td>
                <td><c:out value="${row.admin_code3}"/></td>
                <td><c:out value="${row.latitude}"/></td>
                <td><c:out value="${row.longitude}"/></td>
                <td><c:out value="${row.accuracy}"/></td>
              </tr>
              </c:forEach>
            </table>
          </c:when>
          <c:otherwise>
            <form action="country.jsp" method=POST>
              <label>Code </label><input type="text" name="code">
              <label>Name </label><input type="text" name="name">
              <input type="submit" value="Search">
            </form>
          </c:otherwise>
        </c:choose>
    </body>
</html>
