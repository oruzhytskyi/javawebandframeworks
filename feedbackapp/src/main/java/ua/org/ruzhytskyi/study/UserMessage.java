package ua.org.ruzhytskyi.study;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


public class UserMessage {
    @Pattern(regexp="\\(0[0-9][0-9]\\)[0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9]")
    private String phone;

    @Pattern(regexp="[a-zA-Z0-9\\._%\\+-]+@[a-zA-Z0-9\\.-]+\\.[a-zA-Z]{2,4}")
    private String email;

    @NotNull
    private String message;

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;  
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;  
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;  
    }
}
