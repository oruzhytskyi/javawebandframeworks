package ua.org.ruzhytskyi.study;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Message {
    @Id
    @GeneratedValue
    private Long id;
    private String phone;
    private String email;
    private String message;

    public Message() {
    }

    public Message(String phone, String email, String message) {
        this.phone = phone;
        this.email = email;
        this.message = message;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;  
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;  
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;  
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;  
    }
}
