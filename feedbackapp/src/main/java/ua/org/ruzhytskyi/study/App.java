package ua.org.ruzhytskyi.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


@EnableAutoConfiguration
@Configuration
@ImportResource({"classpath*:applicationContext.xml"})
@SpringBootApplication
public class App 
{
    public static void main( String[] args ) throws Exception {
        SpringApplication.run(App.class, args);
    }
}
