package ua.org.ruzhytskyi.study;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MessagesDAO extends CrudRepository<Message, Long> {
}
