package ua.org.ruzhytskyi.study;

import javax.validation.Valid;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Controller
public class WebController extends WebMvcConfigurerAdapter {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/congrats").setViewName("congrats");
    }

    @RequestMapping(value="/", method=RequestMethod.GET)
    public String showForm(UserMessage userMessage) {
        return "form";
    }

    @RequestMapping(value="/", method=RequestMethod.POST)
    public String checkPersonInfo(
            @Valid UserMessage userMessage, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "form";
        }
        MessagesDAO dao = context.getBean(MessagesDAO.class);
        Message msg = new Message(
            userMessage.getPhone(),
            userMessage.getEmail(),
            userMessage.getMessage()
        );
        dao.save(msg);
        return "redirect:/congrats";
    }
}
